### About
Приложение-парсер информации о матчах через API с взаимодействием через консоль.
Приложение реализовать на фреймворке Laravel. В минимальном функционале приложение
должно парсить информацию с API сайта https://api.pandascore.co информацию о матчах по играм и
сохранять в бд Postgresql.

PS:
на текущий момент изменилась API(структура ответов). Внесены изменения в конфиг парсера.
Актуальными опциями для парсера остались 'csgo'
### Configuration:



.env file:
````
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:Qm8ylefGQfZpp5sT9TAwEj/iBheYTA6TfbI9NYs+9J4=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=pgsql
DB_HOST=172.20.0.5
DB_PORT=5432
DB_DATABASE=score
DB_USERNAME=postgres
DB_PASSWORD=mysecretpassword

BROADCAST_DRIVER=redis
CACHE_DRIVER=redis
QUEUE_CONNECTION=redis
SESSION_DRIVER=redis
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MATCH_PARSER_SOURCE=App\Services\Matches\Sources\PandaScoreApiSource
MATCH_PARSER_STORAGE=App\Services\Matches\Storages\PgsqlDbStorage
MATCH_PARSER=App\Services\Matches\Parsers\PandaScoreMatchParser

PANDA_SCORE_QUERY_TOKEN={YOUR TOKEN HERE}

MIX_PUSHER_APP_KEY=
MIX_PUSHER_APP_CLUSTER=
````

## Start parser

````
php artisan parse:matches
````
