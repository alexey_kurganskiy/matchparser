<?php

use App\Services\Matches\Parsers\PandaScoreMatchParser\Filters\{MatchGameFilter, PlayerFilter, TeamFilter};

return [
    // Основные параметры
    'endpoint' => env('PANDA_SCORE_ENDPOINT', 'https://api.pandascore.co'),
    'query_token' => env('PANDA_SCORE_QUERY_TOKEN'),

    // Конфиг парсера
    'parseConfig' => [
        'teams' => [
            'en_callback' => TeamFilter::class,
            'excluded' => ['location', 'modified_at'],
        ],
        'players' => [
            'en_callback' => PlayerFilter::class
        ],
        'videogames' => [
            'source_key' => 'videogame',
        ],
        'leagues' => [
            'source_key' => 'league',
        ],
        'series' => [
            'source_key' => 'serie',
        ],
        'tournaments' => [
            'source_key' => 'tournament',
            'excluded' => ['live_supported', 'prizepool'],
        ],
        'matches' => [
            'excluded' => [
                'games',
                'league',
                'opponents',
                'serie',
                'tournament',
                'videogame',
                'detailed_stats',
                'game_advantage',
                'rescheduled',
            ]
        ],
        'match_games' => [
            'en_callback' => MatchGameFilter::class,
            'excluded' => ['detailed_stats']
        ]
    ],

    'parseOptions' => [
        'page[size]' => 50,
        'page[number]' => 1,
        'sort' => 'id'
    ],
];
