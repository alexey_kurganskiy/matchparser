<?php

return [
    'source' => env('MATCH_PARSER_SOURCE', 'App\Services\Matches\Sources\PandaScoreApiSource'),
    'storage' => env('MATCH_PARSER_STORAGE', 'App\Services\Matches\Storages\PgsqlDbStorage'),
    'parser' => env('MATCH_PARSER', 'App\Services\Matches\Parsers\PandaScoreMatchParser'),
];