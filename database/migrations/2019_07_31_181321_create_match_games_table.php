<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_games', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->boolean('finished')->nullable();
            $table->boolean('forfeit')->nullable();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('video_url')->nullable();
            $table->bigInteger('match_id')->nullable();
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->string('winner_type')->nullable();
            $table->bigInteger('winner_id')->nullable();
            $table->integer('length')->nullable();
            $table->integer('position')->nullable();
            $table->jsonb('winner')->nullable();

            $table->foreign('match_id')->references('id')->on('matches')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_games');
    }
}
