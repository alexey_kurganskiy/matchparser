<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('league_id')->nullable();
            $table->bigInteger('serie_id')->nullable();
            $table->timestamp('modified_at')->nullable();
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->string('winner_type')->nullable();
            $table->bigInteger('winner_id')->nullable();

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('set null');
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('set null');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
