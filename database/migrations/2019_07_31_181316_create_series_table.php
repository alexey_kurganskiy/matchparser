<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->integer('year')->nullable();
            $table->string('full_name')->nullable();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('prizepool')->nullable();
            $table->string('season')->nullable();
            $table->string('winner_type')->nullable();
            $table->bigInteger('winner_id')->nullable();
            $table->bigInteger('league_id')->nullable();
            $table->timestamp('modified_at')->nullable();
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('end_at')->nullable();

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
