<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamp('scheduled_at')->nullable();
            $table->boolean('draw')->nullable();
            $table->boolean('forfeit')->nullable();
            $table->jsonb('live')->nullable();
            $table->string('live_url')->nullable();
            $table->bigInteger('tournament_id')->nullable();
            $table->bigInteger('league_id')->nullable();
            $table->bigInteger('serie_id')->nullable();
            $table->bigInteger('videogame_id')->nullable();
            $table->jsonb('results')->nullable();
            $table->string('match_type')->nullable();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('videogame_version')->nullable();
            $table->timestamp('modified_at')->nullable();
            $table->integer('number_of_games')->nullable();
            $table->bigInteger('winner_id')->nullable();
            $table->jsonb('winner')->nullable();

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('set null');
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('set null');
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('set null');
            $table->foreign('videogame_id')->references('id')->on('videogames')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
