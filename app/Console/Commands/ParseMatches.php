<?php

namespace App\Console\Commands;

use App\Contracts\MatchParserInterface;
use Exception;
use Illuminate\Console\Command;

class ParseMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:matches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse matches';

    /**
     * @var MatchParserInterface
     */
    protected $parser;

    /**
     * Create a new command instance.
     *
     * @param MatchParserInterface $parser
     */
    public function __construct(MatchParserInterface $parser)
    {
        parent::__construct();
        $this->parser = $parser;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // в настоящий момент csgo только доступен
        $gameName = $this->askWithCompletion('Set game for parse:', ['','csgo','data2','lol', 'ow'], 'csgo');

        $this->output->write("\nStart parse ...");
        try {
            $this->parser->parseUpcomingMatches($gameName);
        } catch (Exception $exception) {
            $this->output->error($exception->getMessage());
        }
        $this->output->success("\ndone\n");
    }
}
