<?php


namespace App\Services\Matches\Storages;

use App\Contracts\MatchStorageInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

/**
 * Class PgsqlDbStorage
 *
 * Store items at PostgreSQL DB
 *
 * @package App\Storages
 */
class PgsqlDbStorage implements MatchStorageInterface
{
    /**
     * Store items at pgsql db.
     *
     * @param string $table
     * @param array $items
     * @param array $excludedItemKeys
     * @param array $uniqueKeys  ON CONFLICT columns condition
     * @return bool
     */
    public function insertOrUpdate(string $table, array $items, array $excludedItemKeys=[], array $uniqueKeys=['id']): bool
    {
        $columns = array_keys($items[0]);
        $columnsString = implode('","', array_diff($columns, $excludedItemKeys));
        $values = $this->buildSQLValuesFrom($items, $excludedItemKeys);
        $updates = $this->buildSQLUpdatesFrom($columns, $excludedItemKeys);


        $query = "INSERT into {$table} (\"{$columnsString}\") values {$values}
ON CONFLICT (id) DO UPDATE
SET {$updates};";

        DB::statement($query);
        return $query;
    }

    protected function buildSQLValuesFrom(array $rows, array $excludedItemKeys = []):string
    {
        $valuesArr = [];
        foreach ($rows as $row) {
            Arr::forget($row, $excludedItemKeys);
            $rowValues = "('";
            foreach ($row as $k=>$v) {
                if (is_array($v)) {
                    $row[$k] = json_encode($v);
                }
            }

            $arrayValues = array_values($row);
            array_walk($arrayValues, function (&$item) {
                if (is_string($item))
                    $item = pg_escape_string($item);
            });

            $rowValues .= implode("','", $arrayValues);
            $rowValues .= "')";
            $valuesArr[] = $rowValues;
        }
        return str_replace("''", 'null', implode(',', $valuesArr));
    }

    protected function buildSQLUpdatesFrom($columns, array $exclude)
    {
        $updateString = collect($columns)->reject(function ($column) use ($exclude) {
            return in_array($column, $exclude);
        })->reduce(function ($updates, $column) {
            return $updates .= "\"{$column}\"=EXCLUDED.\"{$column}\",";
        }, '');

        return trim($updateString, ',');
    }

}
