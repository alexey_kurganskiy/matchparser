<?php

namespace App\Services\Matches\Parsers;

use Illuminate\Support\Arr;

class PandaScoreMatchParser extends AbstractMatchParser
{

    public function parseUpcomingMatches(string $gameName): bool
    {
        $items = [];
        $options = config('pandascore.parseOptions');
        while ($items !== null) {
            $items = $this->dataSource->getMatchesUpcoming($gameName, $options);
            if ($items == null)
                return true;
            $itemsCollection = collect($items);

            $results = config('pandascore.parseConfig');
            array_walk($results, function (&$item) {
                if (is_array($item))
                    $item['items'] = [];
            });

            // prepare data:
            $itemsCollection->each(function ($item, $key) use (&$results) {
                foreach ($results as $resultKey => $result) {
                    $f = Arr::get($result, 'en_callback');
                    if ($f) {
                        $filter = new $f();
                        $filter($item, $results);
                    } else {
                        $sourceKey = Arr::get($result, 'source_key', null);
                        if ($sourceKey) {
                            $results[$resultKey]['items'][$item[$sourceKey]['id']] = $item[$sourceKey];
                        } else {
                            $results[$resultKey]['items'][$item['id']] = $item;
                        }
                    }
                }
            });

            // store data:
            foreach ($results as $resKey => $result) {
                if (!empty($results[$resKey]['items']))
                    $this->dataStorage->insertOrUpdate(
                        $resKey,
                        array_values($results[$resKey]['items']),
                        Arr::get($results[$resKey], 'excluded', []),
                        ['id']);
            }

            $options['page[number]']++;
        }

        return true;
    }
}
