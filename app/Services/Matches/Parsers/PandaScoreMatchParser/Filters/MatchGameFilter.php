<?php


namespace App\Services\Matches\Parsers\PandaScoreMatchParser\Filters;


class MatchGameFilter
{
    public function __invoke($item, &$results)
    {
        foreach ($item['games'] as $game) {
            $results['match_games']['items'][$game['id']] = $game;
        }
    }
}
