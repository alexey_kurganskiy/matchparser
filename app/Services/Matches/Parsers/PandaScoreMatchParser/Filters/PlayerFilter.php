<?php


namespace App\Services\Matches\Parsers\PandaScoreMatchParser\Filters;


class PlayerFilter
{
    public function __invoke($item, &$results)
    {
        foreach ($item['opponents'] as $opponent) {
            if ($opponent['type'] === 'Player')
                $results['players']['items'][$opponent['opponent']['id']] = $opponent['opponent'];
        }
    }
}
