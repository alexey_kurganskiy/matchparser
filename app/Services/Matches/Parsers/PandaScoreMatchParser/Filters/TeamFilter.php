<?php


namespace App\Services\Matches\Parsers\PandaScoreMatchParser\Filters;


use Illuminate\Support\Arr;

class TeamFilter
{
    public function __invoke($item, &$results)
    {
        foreach (Arr::get($item, 'opponents') as $opponent) {
            if (Arr::get($opponent, 'type') === 'Team')
                $results['teams']['items'][$opponent['opponent']['id']] = $opponent['opponent'];
        }
    }
}
