<?php


namespace App\Services\Matches\Parsers;


use App\Contracts\MatchParserInterface;
use App\Contracts\MatchStorageInterface;
use App\Contracts\MatchSourceInterface;

abstract class AbstractMatchParser implements MatchParserInterface
{

    /**
     * @var MatchSourceInterface Matches data source.
     */
    protected $dataSource;

    /**
     * @var MatchStorageInterface Matches data storage.
     */
    protected $dataStorage;

    /**
     * MatchParser constructor.
     *
     * @param MatchSourceInterface $dataSource
     * @param MatchStorageInterface $dataStorage
     */
    public function __construct(MatchSourceInterface $dataSource, MatchStorageInterface $dataStorage)
    {
        $this->dataSource = $dataSource;
        $this->dataStorage = $dataStorage;
    }
}