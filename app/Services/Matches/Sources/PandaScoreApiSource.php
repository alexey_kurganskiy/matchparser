<?php

namespace App\Services\Matches\Sources;

use App\Contracts\MatchSourceInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class PandaScoreApiSource implements MatchSourceInterface
{

    protected $client;

    /**
     * PandaScoreService constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $queryToken = config('pandascore.query_token');
        if ($queryToken == null)
            throw new Exception('env variable "PANDA_SCORE_QUERY_TOKEN" not set');
        $this->client = new Client([
            'base_uri' => config('pandascore.endpoint'),
            'headers' => ['Authorization' => "Bearer $queryToken"]
        ]);
    }

    public function getMatchesUpcoming(string $gameName = '', array $options = []): ?array
    {
        $response = $this->client->get("$gameName/matches/upcoming", [RequestOptions::QUERY => $options]);
        $matches = json_decode($response->getBody(), true);
        return count($matches) > 0 ? $matches : null;
    }
}
