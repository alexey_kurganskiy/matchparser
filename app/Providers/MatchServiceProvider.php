<?php

namespace App\Providers;

use App\Services\Matches\Parsers\PandaScoreMatchParser;
use App\Services\Matches\Sources\PandaScoreApiSource;
use App\Services\Matches\Storages\PgsqlDbStorage;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class MatchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Contracts\MatchParserInterface', function (Application $app) {
            return $app->make(config('matchparser.parser', PandaScoreMatchParser::class));
        });
        $this->app->singleton('App\Contracts\MatchSourceInterface', function (Application $app) {
            return $app->make(config('matchparser.source', PandaScoreApiSource::class));
        });
        $this->app->singleton('App\Contracts\MatchStorageInterface', function (Application $app) {
            return $app->make(config('matchparser.storage', PgsqlDbStorage::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
