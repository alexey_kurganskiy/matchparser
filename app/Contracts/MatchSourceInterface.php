<?php


namespace App\Contracts;


interface MatchSourceInterface
{
    /**
     * Get matches array
     *
     * @param string $gameName
     * @param array $options Options for select matches
     * @return array|null
     */
    public function getMatchesUpcoming(string $gameName = '', array $options = []): ?array;
}