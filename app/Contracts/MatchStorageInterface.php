<?php


namespace App\Contracts;


/**
 * Interface EntityStorageInterface
 *
 * Any storage interface for store entities
 *
 * @package App\Contracts
 */
interface MatchStorageInterface
{
    /**
     * @param string $targetName Target name for store. etc filename, table name or other.
     * @param array $items Associative array of items to store
     * @param array $excludedItemKeys Item keys array for excluding from insert/update data
     * @param array $uniqueKeys Unique item keys
     * @return bool
     */
    public function insertOrUpdate(string $targetName, array $items, array $excludedItemKeys, array $uniqueKeys): bool;
}