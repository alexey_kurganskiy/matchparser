<?php


namespace App\Contracts;


/**
 * Interface MatchParserInterface
 *
 *
 * @package App\Contracts
 */
interface MatchParserInterface
{
    public function parseUpcomingMatches(string $gameName): bool;
}